import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        int res;
        if (liste.size() == 0)
            return null;
        else {
            res = liste.get(0);
            for (int i = 1; i<liste.size(); i++){
                if (liste.get(i)<res)
                    res = liste.get(i);
            }
        }
        return res;
    }
    
    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for(T element: liste){
            if (element.compareTo(valeur)<=0)
                return false;
        }
        
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> list = new ArrayList<T>();
        
        for (T element : liste1){
            if(liste2.contains(element)){
                if (!list.contains(element))
                    list.add(element);
            }
        }
        return list;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        String[] mots = texte.split(" ");
        List<String> listeMot = new ArrayList<String>();       
        for(String element:mots)
            if (element.compareTo("")!=0)
                listeMot.add(element);
        
        return listeMot;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        List<String> liste = decoupe(texte);
        Map<String, Integer> occurence = new HashMap<String, Integer>();
        
        if(liste.isEmpty())
            return null;
        else{
            for (String element :liste){ // boucle qui compte chaque occurence des mots présents
                if(occurence.containsKey(element)){
                    occurence.replace(element, occurence.get(element)+1);               
                }
                else
                    occurence.put(element,1);
            }
        }
        
        String res = "";
        int nbOccurence = 0;
        for(Entry<String, Integer> eltMap : occurence.entrySet()){
            if(res.equals("")){
                res = eltMap.getKey();
                nbOccurence  = eltMap.getValue();
            }
            else{
                if(eltMap.getValue()>nbOccurence){
                    res = eltMap.getKey();
                    nbOccurence  = eltMap.getValue();
                }
                else{
                    if(eltMap.getValue()==nbOccurence){
                        if (eltMap.getKey().compareTo(res)<0){
                            res = eltMap.getKey();
                            nbOccurence  = eltMap.getValue();
                        }
                    }
                }    
            }
        }
        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        
        boolean res = true;
        int cpt = 0;
        
        for(int i =0; i < chaine.length();i++){ // on parcourt toute la chaine
            if(chaine.charAt(i)== '('){   // si parenthese ouvrante on incrémente
                cpt++;
            }
            else{
                if(chaine.charAt(i)== ')'){  // si parenthese fermante on décrémente
                    cpt--;
                }    
            }            
            if(cpt<0){ // si le compteur est inférieure a 0, trop de fermantes on break
                res= false;
                break;
            }               
        }
        if (cpt != 0) // si cpt pas a 0 a la fin du texte, tout n'est pas fermé
            res = false;
        
        return res;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        boolean res = true;
        int cptP = 0;
        int cptC = 0;
        
        for(int i =0; i < chaine.length();i++){ // on parcourt toute la chaine
            if(chaine.charAt(i)== '('){   // si parenthese ouvrante on incrémente
                cptP++;
            }
            else{
                if(chaine.charAt(i)== ')'){  // si parenthese fermante on décrémente
                    cptP--;
                } 
                if(chaine.charAt(i)== '['){   // si parenthese ouvrante on incrémente
                    cptC++;
                }
                else{
                    if(chaine.charAt(i)== ']'){  // si parenthese fermante on décrémente
                        cptC--;
                    }
                }
            }            
            if(cptC<0 || cptP<0){ // si le compteur est inférieure a 0, trop de fermantes on break
                res= false;
                break;
            }               
        }
        if (cptC != 0 || cptP !=0) // si cpt pas a 0 a la fin du texte, tout n'est pas fermé
            res = false;
        
        return res;
    

    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        boolean trouve = false;
        int indDebut = 0;
        int indFin = liste.size();
        int indMilieu;
        
        if(!liste.isEmpty()){
            if(liste.get(indDebut) == valeur || liste.get(indFin-1) == valeur)
                trouve = true;

            while(!trouve && ((indFin-indDebut)>1)){

                indMilieu = (indFin+indDebut)/2;
                if (liste.get(indMilieu)== valeur)
                    trouve = true;
                else{
                    if (liste.get(indMilieu)> valeur)
                        indFin = indMilieu;                
                    else
                        indDebut = indMilieu;
                }                     
            }      
        }     
        return trouve;
    }



}
